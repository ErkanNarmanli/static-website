jQuery(document).ready(function() {
    /* VARIABLE */
    var slug_input  = $('#id_slug')
    var titre_input = $('#id_titre')
    /* EVENTS */
    // on associe paste, keyup et input à do_sggestion
    titre_input.on('paste keyup input',do_suggestion);
    slug_input.change(function() {
        if($(this).val()){$(this).removeClass('slug-suggest')}
    });
    slug_input.blur(function() {
        if(!$(this).val()){$(this).addClass('slug-suggest')}
    });
    /* FONCTIONS */
    // converti une chaine de caractères en slug
    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[èéêë]/g,"e")
            .replace(/[àáâãäå]/g,"a")
            .replace(/[ìíîï]/g,"i")
            .replace(/[òóôõö]/g,"o")
            .replace(/[ùúûü]/g,"u")
            .replace(/ç/g,"c")
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'_');
    }
    function do_suggestion()
    {
        if(slug_input.hasClass('slug-suggest'))
        {
            var slug_raw = convertToSlug(titre_input.val());
            var slug = slug_raw
            var i = 2;
            while(slug_list.indexOf(slug) != -1){
                slug = slug_raw + "_" + i;
                i++;
            }
            slug_input.val(slug);
        }
    }
});
