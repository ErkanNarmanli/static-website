jQuery(document).ready(function() {
    /* VARIABLES */
    var fold_btn        = $('.fold-btn');
    var fold_up_all     = $('.fold-up-all-btn');
    var fold_down_all   = $('.fold-down-all-btn');
    var fold_banner = $('.fold-head').children('.banner-title');
    var down_class  = 'fa-angle-down'; // à changer si nécessaire
    var up_class    = 'fa-angle-up'; // à changer si nécessaire
    /* EVENT */
    fold_btn.on('click', foldSwap); 
    fold_up_all.on('click', foldUpAll);
    fold_down_all.on('click', foldDownAll);
    fold_banner.on('click', foldBanner);
    /* FONCTIONS */
    function foldBanner() {
        var btn = $(this).parent('.fold-head').find('.fold-btn').eq(0);
        btn.trigger('click');
    }
    function foldUpAll() {
        var container  = $(this).parents('.fold-container').eq(0);
        var containers = container.children('.fold-body').find('.fold-container');
        containers.each(function(index) {
            var body = $(this).children('.fold-body');
            var btn  = $(this).children('.fold-head').find('.fold-btn').eq(0);
            if( btn.hasClass(up_class) ){
                btn.trigger('click');
            }
        });
    }
    function foldDownAll() {
        var container  = $(this).parents('.fold-container').eq(0);
        var containers = container.children('.fold-body').find('.fold-container');
        containers.each(function(index) {
            var body = $(this).children('.fold-body');
            var btn  = $(this).children('.fold-head').find('.fold-btn').eq(0);
            if( btn.hasClass(down_class) ){
                btn.trigger('click');
            }
        });
    }
    function foldSwap() {
        $(this).toggleClass(up_class); 
        $(this).toggleClass(down_class); 
        var container = $(this).parents('.fold-container').eq(0);
        container.children('.fold-body').slideToggle('fast'); 
    }
    /* INITIALISATION */
    fold_btn.each(function(index) {
        if( $(this).hasClass(down_class)) {
            var container = $(this).parents('.fold-container').eq(0);
            container.children('.fold-body').slideUp('fast');
        }
    });
});
