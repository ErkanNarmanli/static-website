/* À besoin des variables suivantes :
 * chapitre_url : contient l'url de la liste des chapitres
 * add_chapitre_url : contient l'url du formulaire de création de chapitre
 */
jQuery(document).ready(function() {
    /* CHAPITRES */
    var btn_add_chapitre  = $('#add_chapitre_btn');
    var chapitre_list     = $('#chapitre-list');
    var chapitre_add      = $('#add_chapitre');
    // On charge au début
    chapitre_list.load(chapitre_url); 
    chapitre_add.load(add_chapitre_url);
    // On écoute le bouton 'ajouter'
    btn_add_chapitre.on('click', function() {
        var titre_content = $('#id_chap_titre').val();
        var partie_content = $('#id_partie').val();
        var csrf_value  = $('#add_chapitre').find('[name=csrfmiddlewaretoken]').val();
        if(partie_content && titre_content) {
            $.post(
                add_chapitre_url,
                {   chap_titre   : titre_content,
                    partie       : partie_content,
                    csrfmiddlewaretoken : csrf_value
                },
                function() {
                    // loool
            });
        }
        $('#id_chap_titre').val("");
        $('#id_partie').val("");
        chapitre_list.load(chapitre_url); 
    });
});
