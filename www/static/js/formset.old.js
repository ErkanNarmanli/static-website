var django = {
    "jQuery": jQuery.noConflict(true)
};

(function($) {
    cloneMore = function(selector, type) {
        var newElement = $(selector).clone(true);
        var total = $('#id_' + type + '-TOTAL_FORMS').val();
        newElement.find(':input').each(function() {
            var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        });
        newElement.find('label').each(function() {
            var newFor = $(this).attr('for').replace('-' + (total-1) + '-','-' + total + '-');
            $(this).attr('for', newFor);
        });
        total++;
        $('#id_' + type + '-TOTAL_FORMS').val(total);
        $(selector).after(newElement);
    }
    deleteButtonHandler = function(elem) {
        elem.bind("click", function() {
            var deleteInput = $(this).prev().prev(),
                form = $(this).parents(".dynamic-form").first();
            // callback
            // toggle options.predeleteCssClass and toggle checkbox
            if (form.hasClass("has_original")) {
                form.toggleClass("predelete");
                if (deleteInput.attr("checked")) {
                    deleteInput.attr("checked", false);
                } else {
                    deleteInput.attr("checked", true);
                }
            }
            // callback
        });
    }; 
    $(document).ready(function($) {
        deleteButtonHandler($("table#exo_formset tbody.exo_formset_content").find("a.remove-btn"));
        $("table#exo_formset tbody.exo_formset_content").sortable({
            handle: "a.drag-btn",
            items: "tr",
            axis: "y",
            appendTo: 'body',
            forceHelperSize: true,
            placeholder: 'ui-sortable-placeholder',
            forcePlaceholderSize: true,
            containment: 'form#exo_form',
            tolerance: 'pointer',
            start: function(evt, ui) {
                var template = "",
                    len = ui.item.children("td").length;
                for (var i = 0; i < len; i++) {
                    template += "<td style='height:" + (ui.item.outerHeight() + 12 ) + "px' class='placeholder-cell'>&nbsp;</td>"
                }
                template += "";
                ui.placeholder.html(template);
            },
            stop: function(evt, ui) {
                // Toggle div.table twice to remove webkits border-spacing bug
                $("table#exo_formset").toggle().toggle();
            },
        });
        $("#exo_form").bind("submit", function(){
            var sortable_field_name = "priority";
            var i = 1;
            $(".exo_formset_content").find("tr").each(function(){
                var fields = $(this).find("td :input[value]"),
                    select = $(this).find("td select");
                if (select.val() && fields.serialize()) {
                    $(this).find("input[name$='"+sortable_field_name+"']").val(i);
                    i++;
                }
            });
        });
        
    });
})(django.jQuery);
