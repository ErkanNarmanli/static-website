from jinja2 import Environment, FileSystemLoader

file_outputs = "www/"
file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

class Page:
    def __init__(self, filename):
        self.filename = filename
        self.filename_out = file_outputs+filename

    def render(self):
        template = env.get_template(self.filename)
        # output = template.render(content=content)
        output = template.render()
        with open(self.filename_out, "w") as fh:
            fh.write(output)

## HOME.HTML
# content = 'This is about page'
for file in ['index.html','enseignements.html','arts.html','recherche.html','associatif.html','cv.html']:
    page = Page(file)
    page.render()